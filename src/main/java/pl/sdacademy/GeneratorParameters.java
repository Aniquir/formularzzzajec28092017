package pl.sdacademy;

import lombok.Data;

/**
 * Created by RENT on 2017-09-28.
 */
//dodaje sama getery setter, construktory dla pol finalnych i inne, doczytaj "project lombok"
@Data
public class GeneratorParameters {

    private int lenght;
    private boolean lowercase;
    private boolean symbols;
    private boolean numbers;
    private boolean excludeSimilar;


}
