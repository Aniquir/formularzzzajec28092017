package pl.sdacademy;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by RENT on 2017-09-28.
 */
@WebServlet ("/obslugaGeneratora")
public class ObslugaGeneratoraServlet extends HttpServlet{

    private PasswordGenerator passwordGenerator;
    @Override
    public void init() throws ServletException {
        passwordGenerator = new PasswordGenerator();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");


        GeneratorParameters parameters = createParameters(req);
        int liczbaHasel = pobierzParametr(req,"liczbaHasel");

        List<String> passwords = passwordGenerator.generate(liczbaHasel, parameters);

        resp.setContentType("text/html; charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        out.println("<h1>Wygenerowane hasla:</h1>");

        out.println("<ol>");
        for (String password : passwords){
           out.println("<li>" + password + "</li>");
        }
        out.println("</ol>");
    }

    private boolean checkboxToBoolean(String checkbox){
        return "on".equals(checkbox);
    }

    private GeneratorParameters createParameters(HttpServletRequest request) {
        GeneratorParameters parameters = new GeneratorParameters();

        parameters.setLenght(pobierzParametr(request, "Dlugosc"));
        parameters.setLowercase(checkboxToBoolean(request.getParameter("maleLitery")));
        parameters.setSymbols(checkboxToBoolean(request.getParameter("znakiSpecjalne")));
        parameters.setNumbers(checkboxToBoolean(request.getParameter("cyfry")));
        parameters.setExcludeSimilar(checkboxToBoolean(request.getParameter("wykluczPodobne")));


        return parameters;
    }

    private int pobierzParametr(HttpServletRequest request, String parameterName) {
        try {
            return Integer.parseInt(request.getParameter(parameterName));
        } catch (NumberFormatException exception) {
            return 1;
        }
    }
}
