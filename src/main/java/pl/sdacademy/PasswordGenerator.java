package pl.sdacademy;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by RENT on 2017-09-28.
 */

public class PasswordGenerator {

    private static final String UPPERCASE_CHARACTERS = "QAZXSWEDCVFRTGBNHYUJMKILOP";
    private static final String LOWERCASE_CHARACTERS = "qazxswedcvfrtgbnhyujmkiolp";
    private static final String SYMBOLS = "!@#$%^&*()[]{}-=";
    private static final String NUMBERS = "1234567890";
    private static final String EXCLUDED = "iIoOlL10";

    //

    private Random generator;

    public PasswordGenerator() {
        this.generator = new Random();
    }

    public List<String> generate(int count, GeneratorParameters parameters){
        List<String> passwords = new ArrayList<>();

        for (int i = 0; i < count; i++){
            passwords.add(generate(parameters));
        }

        return passwords;
    }
//generuje haslo skladajace sie tylko z wielkich liter
    private String generate (GeneratorParameters parameters){
        StringBuilder alphabet = new StringBuilder(UPPERCASE_CHARACTERS);
        if (parameters.isLowercase()){
            alphabet.append(LOWERCASE_CHARACTERS);
        }
        if (parameters.isNumbers()){
            alphabet.append(NUMBERS);
        }
        if (parameters.isSymbols()) {
            alphabet.append(SYMBOLS);
        }
        if (parameters.isExcludeSimilar()) {
            excludeSimilar(alphabet);
        }
        String password = "";
        for (int i = 0; i < parameters.getLenght(); i++){
            password += alphabet.charAt(generator.nextInt(alphabet.length()));
        }

        return password;
    }

    private void excludeSimilar(StringBuilder alphabet) {
//        iIoOlL10
             for( int i = 0; i < EXCLUDED.length(); i++){
                 String excludedCharacter = EXCLUDED.substring(i, i + 1);
                 int index = alphabet.indexOf(excludedCharacter);
                 alphabet.deleteCharAt(index);
                 //to sam w jednej linijce v
//                 alphabet.deleteCharAt(alphabet.indexOf(EXCLUDED.substring(i, i + 1)));
             }
    }
}
