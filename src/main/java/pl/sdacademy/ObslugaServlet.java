package pl.sdacademy;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

@WebServlet("/obslugaFormularza")
public class ObslugaServlet extends HttpServlet {

    private String nullToEmpty(String text) {
        return text == null ? "" : text;
    }

    /**
     * Metoda zamienia wartosc checkboxa na wartosc logiczna
     *
     * @param checkbox
     * @return true, jezeli checkbox=on, false, w kazdym  innym przypadku
     */
    private boolean checkboxToBoolean(String checkbox){
        return "on".equals(checkbox);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        out.println("<h1>Obsługa formularza</h1>");

        String imie = req.getParameter("imie");
        String nazwisko = req.getParameter("nazwisko");
        String haslo = req.getParameter("haslo");
        boolean czyJsp = checkboxToBoolean(req.getParameter("jsp"));
        boolean czyJstl = checkboxToBoolean(req.getParameter("jstl"));
        String wybor = req.getParameter("wybor");

        out.println("<ul>");

        out.println("<li>imie: " + imie + "</li>");
        out.println("<li>nazwisko: " + nazwisko + "</li>");
        out.println("<li>haslo: " + haslo + "</li>");
        out.println("<li>jsp: " + czyJsp + "</li>");
        out.println("<li>jstl: " + czyJstl + "</li>");
        out.println("<li>wybor: " + wybor + "</li>");

        out.println("</ul>");
    }
}
